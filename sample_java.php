<!DOCTYPE html>
<html>
<body>
<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title><CodeReader></title>

    <!-- Bootstrap Core CSS -->
    <link href="css/bootstrap.min.css" rel="stylesheet">

    <!-- Custom CSS -->

    <!-- Custom Fonts -->
    <link href="font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
    <style type="text/css"> 
        body{width: 100%; background-color: #e6e6e6;}
        #header {height:100px; width:100%;background-color: #2c3e50; color:#18bc9c;}
        #header h1{font-size: 3.5em; text-align: center; font-family:monospace;}
        .content h6{font-size: 1.5em; text-align: center; font-family:monospace; margin-left: -14%; }
        .content p{font-size: 1em; text-align: center; font-family:monospace; color:#A1A1A1; margin-left: -11%; }
        .content{ color:#333; font-family: cursive; text-align: center;}
.content button {width:250px;}
    }
}
.result {
    width: 600px;
}
</style>
</head>

<body>
    <nav class="">
        <div class="container" id="header">
           <div class="row" >
              <div class="col-md-12"><h1>CodeReader</h1></div>
          </div>
      </div>
  </nav>
  <br/>
  <div class="container">
    <div class="row">
    <form action="upload.php" method="post" enctype="multipart/form-data">
        <div class="col-md-12 content"> 
           <h3>Select your image:</h3>
           <br/>
           <input type="file" class="btn btn-primary" name="fileToUpload" id="fileToUpload" >
           <div class="col-md-12 content"> 
           <h3>Upload image:</h3>
           <br/>
           <input type="submit" class="btn btn-primary" value="Upload Image" name="submit">
        </div>
    </form>
           <div class="result">
               <?php
                    require_once 'TesseractOCR/TesseractOCR.php';
                    if (isset($_GET['name'])) {
                        $tesseract = new TesseractOCR($_GET['name']);
                        $tesseract->setWhitelist(range('A','Z'), range('a', 'z'), range(0,9), '_-.,;"#<>()%{}[]= ');
                        $txt = $tesseract->recognize();
                        $myfile = fopen("newfile.java", "w") or die("Unable to open file!");
                        fwrite($myfile, $txt);
                        fclose($myfile);
                ?>
                      <div class="recognized-text">
                      <form method="post" action="resubmit.php">
                        <textarea id="edit-text" name="edit-text" cols="50" rows="10"><?php print_r(htmlspecialchars($txt)); ?></textarea>
                        <input type="submit" class="btn btn-primary" value="Resubmit" name="resubmitj">
                      </form>
                      </div>
                      <?php 
                      echo "<br>-------------------------------------------------------------------------<br>";
                        // ---------- Execute ---------- //
                          exec("javac newfile.java", $c, $check);
                          if (!$check) {
                            exec("java A", $output);
                            print_r($output);
                          }
                          else {
                            system("javac newfile.java 2>&1");
                          }
                    }
                      ?>
           </div>
        </div>
        <br/><br/><br/><br/>
   </div>
</div>




</body>
</html>
<!-- <form action="upload.php" method="post" enctype="multipart/form-data">
    Select image to upload:
    <input type="file" name="fileToUpload" id="fileToUpload">
    <br>
    <input type="submit" value="Upload Image" name="submit">
</form>
 -->
</body>
</html>
